# Ofgem

Prototype of Ofgem infrastructure

# Credentials

- [Install Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
- `az login`. This will return an array with `id` in - this is your `SUBSCRIPTION_ID`
- `az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/${SUBSCRIPTION_ID}"`

This will return an object. Set values as envvars in Terraform Cloud:
- `SUBSCRIPTION_ID` to `ARM_SUBSCRIPTION_ID`
- `tenant` to `ARM_TENANT_ID`
- `password` to `ARM_CLIENT_SECRET`
- `appId` to `ARM_CLIENT_ID`

This should now allow you to run using Terraform Cloud
