resource "azurerm_resource_group" "mysql" {
  name = "${var.prefix}-mysql-resources"
  location = var.location
}

resource "azurerm_mariadb_server" "mysql" {
  name = "${var.prefix}-mysql"
  location = azurerm_resource_group.mysql.location
  resource_group_name = azurerm_resource_group.mysql.name

  sku_name = "B_Gen5_2"

  storage_profile {
    storage_mb = 5120
    backup_retention_days = 7
    geo_redundant_backup = "Disabled"
  }

  administrator_login = var.mysql_user
  administrator_login_password = var.mysql_pass
  version = "10.3"
  ssl_enforcement = "Disabled"
}

resource "azurerm_mariadb_configuration" "mysql" {
  resource_group_name = azurerm_resource_group.mysql.name
  server_name = azurerm_mariadb_server.mysql.name
  value = "+00:00"
  name = "time_zone"
}

resource "azurerm_mariadb_database" "mysql" {
  name = "${var.prefix}_mysql"
  resource_group_name = azurerm_resource_group.mysql.name
  server_name = azurerm_mariadb_server.mysql.name
  charset = "utf8"
  collation = "utf8_general_ci"
}

resource "azurerm_mariadb_firewall_rule" "azure_service_access" {
  name = "${var.prefix}-azure-access"
  resource_group_name = azurerm_resource_group.mysql.name
  server_name =azurerm_mariadb_server.mysql.name
  start_ip_address = "0.0.0.0"
  end_ip_address = "0.0.0.0"
}


resource "azurerm_mariadb_firewall_rule" "office" {
  name = "${var.prefix}-office"
  resource_group_name = azurerm_resource_group.mysql.name
  server_name =azurerm_mariadb_server.mysql.name
  start_ip_address = var.mysql_firewall_start_ip
  end_ip_address = var.mysql_firewall_end_ip
}
