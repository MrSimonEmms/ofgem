variable "prefix" {
  description = "A prefix used for all resources in this example"
}

variable "location" {
  description = "The Azure Region in which all resources in this example should be provisioned"
}

variable "service_principal_client_id" {
  description = "The Client ID for the Service Principal to use for this"
}

variable "service_principal_client_secret" {
  description = "The Client Secret for the Service Principal to use for this"
}

variable "gitlab_token" {
}

variable "gitlab_projectId" {
}

variable "mysql_firewall_start_ip" {
}

variable "mysql_firewall_end_ip" {
}

variable "mysql_user" {
}

variable "mysql_pass" {
}
