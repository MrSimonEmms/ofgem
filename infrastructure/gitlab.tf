provider "gitlab" {
  token = var.gitlab_token
  version = "~> 2.5"
}

resource "gitlab_project_variable" "kubeconfig" {
  key = "KUBECONFIG"
  project = var.gitlab_projectId
  value = azurerm_kubernetes_cluster.k8s.kube_config_raw
  variable_type = "env_var"
  protected = true
  environment_scope = "*"
}
