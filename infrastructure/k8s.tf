resource "azurerm_resource_group" "k8s" {
  name = "${var.prefix}-k8s-resources"
  location = var.location
}

resource "azurerm_kubernetes_cluster" "k8s" {
  name = "${var.prefix}-k8s"
  location = azurerm_resource_group.k8s.location
  resource_group_name = azurerm_resource_group.k8s.name
  dns_prefix = "${var.prefix}-k8s"
  kubernetes_version = "1.15.7"

  default_node_pool {
    name = "default"
    node_count = 3
    vm_size = "Standard_D1_v2"
  }

  service_principal {
    client_id = var.service_principal_client_id
    client_secret = var.service_principal_client_secret
  }

  tags = {
    Environment = "Test"
  }
}
